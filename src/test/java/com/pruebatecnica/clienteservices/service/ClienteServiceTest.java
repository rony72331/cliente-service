package com.pruebatecnica.clienteservices.service;

import com.pruebatecnica.clienteservices.entity.Cliente;
import com.pruebatecnica.clienteservices.request.ClienteRequest;
import com.pruebatecnica.clienteservices.execption.LocalNotFoundException;
import com.pruebatecnica.clienteservices.respository.CienteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ClienteServiceTest {

    @InjectMocks
    private ClienteServiceImp clienteService;

    @Mock
    private CienteRepository clienteRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetClienteValid() throws LocalNotFoundException {
        // Configuración de datos de prueba
        ClienteRequest clienteRequest = new ClienteRequest("C", "23445322");
        Cliente clienteMock = new Cliente();
        clienteMock.setPrimerNombre("Rony");
        clienteMock.setSegundoNombre("Daniel");
        clienteMock.setPrimerApellido("Mendoza");
        clienteMock.setSegundoApellido("Mendoza");
        clienteMock.setTelefono("3023516106");
        clienteMock.setDireccion("Carrera 16b #55-14");
        clienteMock.setCiudadResidencia("Soledad");

        Mockito.when(clienteRepository.getClienteMock("23445322", "C")).thenReturn(clienteMock);

        // Ejecutar el método bajo prueba
        ResponseEntity<Cliente> responseEntity = clienteService.getCliente(clienteRequest);

        // Verificar que el cliente se encontró y la respuesta es exitosa (HTTP 200)
        assertEquals(200, responseEntity.getStatusCodeValue());
        assertEquals(clienteMock, responseEntity.getBody());
    }

    @Test
    public void testGetClienteNotFound() {
        // Configuración de datos de prueba para un cliente no encontrado
        ClienteRequest clienteRequest = new ClienteRequest("C", "12345678");

        Mockito.when(clienteRepository.getClienteMock("12345678", "C")).thenReturn(null);

        // Ejecutar el método bajo prueba y verificar que se lanza una excepción LocalNotFoundException
        assertThrows(LocalNotFoundException.class, () -> clienteService.getCliente(clienteRequest));
    }
}