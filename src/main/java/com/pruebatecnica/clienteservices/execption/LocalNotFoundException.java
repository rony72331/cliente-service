package com.pruebatecnica.clienteservices.execption;

public class LocalNotFoundException extends Exception{
    public LocalNotFoundException(String message) {
        super(message);
    }
}