package com.pruebatecnica.clienteservices.controller;

import com.pruebatecnica.clienteservices.entity.*;

import com.pruebatecnica.clienteservices.request.ClienteRequest;
import com.pruebatecnica.clienteservices.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping("/getCliente")
    public ResponseEntity<Cliente>  getCliente(@Valid @RequestBody ClienteRequest clienteRequest) throws Exception {
     return clienteService.getCliente(clienteRequest);
    }

}



