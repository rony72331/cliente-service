package com.pruebatecnica.clienteservices.service;


import com.pruebatecnica.clienteservices.entity.Cliente;
import com.pruebatecnica.clienteservices.request.ClienteRequest;
import com.pruebatecnica.clienteservices.execption.LocalNotFoundException;
import com.pruebatecnica.clienteservices.respository.CienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImp implements ClienteService{

    @Autowired
    CienteRepository clienteRepository;
    private static final Logger logger = LoggerFactory.getLogger(ClienteServiceImp.class);

    @Override
    public ResponseEntity<Cliente> getCliente(ClienteRequest clienteRequest) throws LocalNotFoundException{
            logger.info("request: " + clienteRequest.toString());
            String tipoDocumento = clienteRequest.getTipoDocumento();
            String numeroDocumento = clienteRequest.getNumeroDocumento();
            // Obtener datos del cliente mockeados
            Cliente cliente = clienteRepository.getClienteMock(numeroDocumento, tipoDocumento);

            if (cliente != null) {
                logger.info("response: " + cliente.toString());
                return ResponseEntity.ok(cliente); // Código HTTP 200 - OK
            } else {
                logger.error("No se encontró el número de documento digitado: " + numeroDocumento + ", Para el tipo de documento: " + tipoDocumento);
                throw new LocalNotFoundException("No se encontró el número de documento digitado: " + numeroDocumento + ", Para el tipo de documento: " + tipoDocumento); // Código HTTP 404 - NOT FOUND
            }
    }

}
