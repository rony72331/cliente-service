package com.pruebatecnica.clienteservices.service;

import com.pruebatecnica.clienteservices.entity.Cliente;
import com.pruebatecnica.clienteservices.request.ClienteRequest;
import com.pruebatecnica.clienteservices.execption.LocalNotFoundException;
import org.springframework.http.ResponseEntity;

public interface ClienteService {
    ResponseEntity<Cliente> getCliente(ClienteRequest clienteRequest) throws LocalNotFoundException;
}
