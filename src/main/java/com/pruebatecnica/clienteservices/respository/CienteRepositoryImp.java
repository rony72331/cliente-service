package com.pruebatecnica.clienteservices.respository;

import com.pruebatecnica.clienteservices.entity.Cliente;
import org.springframework.stereotype.Repository;

@Repository
public class CienteRepositoryImp implements CienteRepository {


    @Override
    public Cliente getClienteMock(String numeroDocumento, String tipoDocumento) {
        if ("23445322".equals(numeroDocumento) && ("C".equals(tipoDocumento) || "c".equals(tipoDocumento))) {
            Cliente cliente = new Cliente();
            cliente.setPrimerNombre("Rony");
            cliente.setSegundoNombre("Daniel");
            cliente.setPrimerApellido("Mendoza");
            cliente.setSegundoApellido("Mendoza");
            cliente.setTelefono("3023516106");
            cliente.setDireccion("Carrera 16b #55-14");
            cliente.setCiudadResidencia("Soledad");
            return cliente;
        }
        return null;
    }
}
