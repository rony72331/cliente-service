package com.pruebatecnica.clienteservices.respository;


import com.pruebatecnica.clienteservices.entity.Cliente;

public interface CienteRepository {


    Cliente getClienteMock(String numeroDocumento, String tipoDocumento);
}
