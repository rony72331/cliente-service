package com.pruebatecnica.clienteservices.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClienteRequest {

    @NotBlank(message = "El tipo de docuemnto no puede ser nulo")
    @Pattern(regexp = "^[CP]$", message = "El tipo de documento debe ser 'C' o 'P'.")
    @Size(min = 1, max = 1, message = "El tipo de documento debe tener exactamente un carácter")
    private String tipoDocumento;

    @Pattern(regexp = "\\d+", message = "El número de documento debe contener solo dígitos")
    @NotNull(message = "El número de documento no puede ser nulo")
    private String numeroDocumento;


}
